const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
    const { currency_name, currency_ex } = req.body;

    if (typeof currency_name !== 'string') {
      return res.status(400).send({ error: 'Currency name must be a string' });
    }

    if (currency_name.trim() === '') {
      return res.status(400).send({ error: 'Currency name must not be empty' });
    }

    if (currency_ex && typeof currency_ex !== 'object') {
      return res.status(400).send({ error: 'Currency exchange must be an object' });
    }

    if (currency_ex && Object.keys(currency_ex).length === 0) {
      return res.status(400).send({ error: 'Currency exchange must not be empty' });
    }

    if (currency_ex && !currency_ex.hasOwnProperty(currency_name)) {
      return res.status(400).send({ error: 'Currency alias not found' });
    }

    if (!exchangeRates.hasOwnProperty(currency_name)) {
      return res.status(400).send({ error: 'Currency not found' });
    }

    return res.status(200).send({ rates: exchangeRates[currency_name], submission: 'written' });
  });
}



